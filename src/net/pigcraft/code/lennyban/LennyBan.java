package net.pigcraft.code.lennyban;

import net.pigcraft.code.lennyban.listeners.ChatListener;
import org.bukkit.plugin.java.JavaPlugin;

public final class LennyBan extends JavaPlugin
{

    @Override
    public void onEnable()
    {
        this.getServer().getPluginManager().registerEvents(new ChatListener(), this);
    }

}
