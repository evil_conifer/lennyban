package net.pigcraft.code.lennyban.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public final class ChatListener implements Listener
{

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event)
    {
        if (event.getMessage().contains("(͡° ͜ʖ ͡°)") || event.getMessage().contains("{° ͜ʖ ͡°}"))
        {
            event.getPlayer().setBanned(true);
            event.getPlayer().kickPlayer("Banned by LennyBan!");
            Bukkit.getServer().broadcastMessage("Player " + event.getPlayer().getDisplayName() + " was banned for posting Le Face Face!");
        }
    }

}
